class Wolf {
  private:
    NexPicture *leftPic;
    NexPicture *rightPic;
    int (&sprites)[4];
    int emptyLeftSprite;
    int emptyRightSprite;

  public:
    //Состояния волка: 0 - левый верхний угол, 1 - левый нижний, 2 - правый нижний, 3 - правый верхний
    int state;

    Wolf(NexPicture* lp, NexPicture* rp, int (&spr)[4], int els, int ers) : sprites(spr) {
      leftPic = lp;
      rightPic = rp;
      emptyLeftSprite = els;
      emptyRightSprite = ers;
      state = 0;
    }

    void initState() {
      state = 0;      
      rightPic->setPic(emptyRightSprite);
      leftPic->setPic(sprites[state]);
    }

    void setState(int newState) {
      if (state == newState) {
        return;
      }

      if (state == 0 || state == 1) {
        if (newState == 2 || newState == 3) {
          leftPic->setPic(emptyLeftSprite);
          rightPic->setPic(sprites[newState]);
        } else {
          leftPic->setPic(sprites[newState]);
        }
        state = newState;
        return;
      }

      if (state == 2 || state == 3) {
        if (newState == 0 || newState == 1) {
          rightPic->setPic(emptyRightSprite);
          leftPic->setPic(sprites[newState]);
        } else {
          rightPic->setPic(sprites[newState]);
        }
        state = newState;
        return;
      }
    }
};
