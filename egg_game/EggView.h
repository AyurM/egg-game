class EggView {
  private:
    int (&sprites)[5];
    int (&emptySprites)[5];
    int expectedWolfState;
    NexPicture *picture;

  public:
    EggView(NexPicture pic[5], int (&spr)[5], int (&es)[5], int ews) : sprites(spr), emptySprites(es) {
      picture = pic;
      expectedWolfState = ews;
    }

    void showEgg(Egg egg) {
      //яйцо неактивно
      if (egg.state == -1) {
        return;
      }

      //Убрать спрайт с предыдущей позиции яйца
      if (egg.state <= 5 && egg.state > 0) {
        picture[egg.state - 1].setPic(emptySprites[egg.state - 1]);
      }

      //Показать спрайт на новой позиции яйца
      if (egg.state < 5) {
        picture[egg.state].setPic(sprites[egg.state]);
      }
    }

    void resetState() {
      for (int i = 0; i < 5; i++) {
        picture[i].setPic(emptySprites[i]);
      }
    }
};
