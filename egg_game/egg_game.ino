#include "Nextion.h"
#include "Tft_menu.h"
#include "EggGame.h"

//ESP-12E GPIOs:
const byte BUTTON_A_PIN = 5;    //NodeMCU - D1
const byte BUTTON_B_PIN = 4;    //NodeMCU - D2
const byte BUTTON_C_PIN = 0;    //NodeMCU - D3
const byte BUTTON_D_PIN = 2;    //NodeMCU - D4
const byte TFT_RX_PIN = 14;     //TX от TFT-дисплея в 14, NodeMCU - D5
const byte TFT_TX_PIN = 12;     //RX от TFT в 12, NodeMCU - D6
SoftwareSerial SerialTJC(TFT_RX_PIN, TFT_TX_PIN);

const long DEBOUNCE_TIME = 20;  //защита от дребезга кнопки, мс
volatile unsigned long lastPress = 0;
volatile byte wolfState = 0;
volatile boolean stateChanged = false;
unsigned long lastTick = 0;

Wolf wolf = Wolf(&wolf_left_pic, &wolf_right_pic, wolf_sprites, 4, 5);
const EggView egg0 = EggView(eggs_top_left, eggs_tl, eggs_tl_empty, 0);
const EggView egg1 = EggView(eggs_bottom_left, eggs_bl, eggs_bl_empty, 1);
const EggView egg2 = EggView(eggs_bottom_right, eggs_br, eggs_br_empty, 2);
const EggView egg3 = EggView(eggs_top_right, eggs_tr, eggs_tr_empty, 3);
EggView eggs[4] = {egg0, egg1, egg2, egg3};
Egg e[4] = {Egg(), Egg(), Egg(), Egg()};
EggMover em = EggMover(e);
EggGame game = EggGame(eggs, wolf, em);

void setup() {
  //Serial.begin(115200);
  randomSeed(analogRead(A0)); //инициализация ГСЧ
  pinSetup();
  startTFT();
  game.init();
}

void loop() {
  if (stateChanged) {
    game.setWolfState(wolfState);
    stateChanged = false;
  }

  if ((millis() - lastTick) >= game.speed) {
    lastTick = millis();
    game.tick();
  }
}

void pinSetup() {
  pinMode(BUTTON_A_PIN, INPUT_PULLUP);
  pinMode(BUTTON_B_PIN, INPUT_PULLUP);
  pinMode(BUTTON_C_PIN, INPUT_PULLUP);
  pinMode(BUTTON_D_PIN, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(BUTTON_A_PIN), buttonAISR, FALLING);
  attachInterrupt(digitalPinToInterrupt(BUTTON_B_PIN), buttonBISR, FALLING);
  attachInterrupt(digitalPinToInterrupt(BUTTON_C_PIN), buttonCISR, FALLING);
  attachInterrupt(digitalPinToInterrupt(BUTTON_D_PIN), buttonDISR, FALLING);
}

ICACHE_RAM_ATTR void buttonAISR() {
  if ((long)(micros() - lastPress) >= DEBOUNCE_TIME * 1000) {
    wolfState = 0;
    stateChanged = true;
    lastPress = micros();
  }
}

ICACHE_RAM_ATTR void buttonBISR() {
  if ((long)(micros() - lastPress) >= DEBOUNCE_TIME * 1000) {
    wolfState = 1;
    stateChanged = true;
    lastPress = micros();
  }
}

ICACHE_RAM_ATTR void buttonCISR() {
  if ((long)(micros() - lastPress) >= DEBOUNCE_TIME * 1000) {
    wolfState = 2;
    stateChanged = true;
    lastPress = micros();
  }
}

ICACHE_RAM_ATTR void buttonDISR() {
  if ((long)(micros() - lastPress) >= DEBOUNCE_TIME * 1000) {
    wolfState = 3;
    stateChanged = true;
    lastPress = micros();
  }
}
