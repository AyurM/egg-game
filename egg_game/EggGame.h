#include "Egg.h"
#include "EggView.h"
#include "EggMover.h"
#include "Wolf.h"

class EggGame {
  private:
    EggView *eggViews;
    Wolf (&wolf);
    EggMover (&mover);
    int counter;
    //Мин и макс продолжительность присутствия/отсутствия петуха на экране (в тактах игры)
    int minCockDuration = 20;
    int maxCockDuration = 28;
    int lastCockChange = 0;
    int cockDuration = 0;

    int startSpeed = 350; //1 игровой такт = 350 мс
    int maxSpeed = 200;

    bool inputBlocked;
    bool cock;

    //для анимации половины промаха
    bool halfScoreIsShown = false;  
    int missAnimationTick = 0;

    void onEggCatch() {
      score++;
      showScore();

      if (score == 5 || score == 10 || score == 60) {
        mover.increaseMaxEggs();
      }

      //увеличение темпа игры через каждые 50 очков
      if(score % 50 == 0 && speed != maxSpeed){
        speed -= 50;
      }

      //сброс штрафных очков
      if (score == 200 || score == 500) {
        miss = 0;
        showMiss();
      }
    }

    void onEggMiss(int direction) {
      inputBlocked = true;
      mover.onMiss();
      resetEggViews();

      //если из окна торчал петух :)
      if (cock) {
        miss++;
        cock = false;
        showCock();
        lastCockChange = counter;
        cockDuration = getCockDuration();
        chickAnimation(direction);
      } else {
        miss += 2;
        brokenEggAnimation(direction);
      }

      showMiss();
      delay(3000);
      if (miss >= 6) {
        delay(3000);
        init();
      }
      inputBlocked = false;
    }

    void showScore() {
      int d1 = score / 100; //кол-во сотен
      int d2 = score / 10;  //кол-во десятков
      if (d2 > 9) {
        d2 = d2 % 10;
      }
      int d3 = score % 10;  //кол-во единиц
      if (d1 > 0) {
        digit_1_pic.setPic(digits[d1]);
      } else {
        digit_1_pic.setPic(digits[10]); //пустая картинка вместо цифры
      }

      if (d1 > 0 || d2 != 0) {
        digit_2_pic.setPic(digits[d2]);
      } else {
        digit_2_pic.setPic(digits[10]); //пустая картинка вместо цифры
      }

      digit_3_pic.setPic(digits[d3]);
    }

    void showMiss() {
      if (miss == 1 || miss == 2) {
        miss_sign_pic.setPic(38);
        miss_1_pic.setPic(40);
      } else if (miss == 3 || miss == 4) {
        miss_1_pic.setPic(40);
        miss_2_pic.setPic(40);
      } else if (miss > 4) {
        //из-за анимации "половины промаха" картинки могут находиться в неактивном состоянии,
        //обновить их заново
        miss_1_pic.setPic(40);
        miss_2_pic.setPic(40);
        miss_3_pic.setPic(40);
      } else {
        miss_sign_pic.setPic(39);
        miss_1_pic.setPic(41);
        miss_2_pic.setPic(41);
        miss_3_pic.setPic(41);
      }
      halfScoreIsShown = true;
      missAnimationTick = counter;
    }

    void animateMiss(){
      if(miss == 0 || miss == 2 || miss == 4 || miss == 6){
        return;
      }
      
      halfScoreIsShown = !halfScoreIsShown;
      
      if(miss == 1){
        miss_1_pic.setPic(halfScoreIsShown ? 40 : 41);
      } else if(miss == 3){
        miss_2_pic.setPic(halfScoreIsShown ? 40 : 41);
      } else if (miss == 5){
        miss_3_pic.setPic(halfScoreIsShown ? 40 : 41);
      }
      missAnimationTick = counter;
    }

    void showCock() {
      cock_pic.setPic(cock ? 86 : 87);
    }

    int getCockDuration() {
      return random(maxCockDuration - minCockDuration + 1) + minCockDuration;
    }

    //анимация появления цыпленка из разбитого яйца
    void chickAnimation(int direction) {
      if (direction == 0 || direction == 1) {
        breakl_1_pic.setPic(58);
        breakl_2_pic.setPic(60);
        breakl_3_pic.setPic(62);
        delay(speed * 2);
        breakl_1_pic.setPic(59);
        breakl_4_pic.setPic(64);
        delay(speed * 2);
        breakl_4_pic.setPic(65);
        breakl_5_pic.setPic(66);
        delay(speed * 2);
        breakl_5_pic.setPic(67);
        breakl_6_pic.setPic(68);
        delay(speed * 2);
        breakl_6_pic.setPic(69);
        breakl_7_pic.setPic(70);
        delay(speed * 2);
        breakl_7_pic.setPic(71);
        breakl_2_pic.setPic(61);
        breakl_3_pic.setPic(63);
      } else {
        breakr_1_pic.setPic(72);
        breakr_2_pic.setPic(74);
        breakr_3_pic.setPic(76);
        delay(speed * 2);
        breakr_1_pic.setPic(73);
        breakr_4_pic.setPic(78);
        delay(speed * 2);
        breakr_4_pic.setPic(79);
        breakr_5_pic.setPic(80);
        delay(speed * 2);
        breakr_5_pic.setPic(81);
        breakr_6_pic.setPic(82);
        delay(speed * 2);
        breakr_6_pic.setPic(83);
        breakr_7_pic.setPic(84);
        delay(speed * 2);
        breakr_7_pic.setPic(85);
        breakr_2_pic.setPic(75);
        breakr_3_pic.setPic(77);
      }
    }

    //анимация разбитого яйца
    void brokenEggAnimation(int direction) {
      if (direction == 0 || direction == 1) {
        breakl_1_pic.setPic(58);
        breakl_2_pic.setPic(60);
        breakl_3_pic.setPic(62);
        delay(speed * 5);
        breakl_1_pic.setPic(59);
        breakl_2_pic.setPic(61);
        breakl_3_pic.setPic(63);
      } else {
        breakr_1_pic.setPic(72);
        breakr_2_pic.setPic(74);
        breakr_3_pic.setPic(76);
        delay(speed * 5);
        breakr_1_pic.setPic(73);
        breakr_2_pic.setPic(75);
        breakr_3_pic.setPic(77);
      }
    }

    void clearBrokenEggs() {
      breakl_1_pic.setPic(59);
      breakl_2_pic.setPic(61);
      breakl_3_pic.setPic(63);
      breakl_4_pic.setPic(65);
      breakl_5_pic.setPic(67);
      breakl_6_pic.setPic(69);
      breakl_7_pic.setPic(71);
      breakr_1_pic.setPic(73);
      breakr_2_pic.setPic(75);
      breakr_3_pic.setPic(77);
      breakr_4_pic.setPic(79);
      breakr_5_pic.setPic(81);
      breakr_6_pic.setPic(83);
      breakr_7_pic.setPic(85);
    }

    void resetEggViews() {
      for (int i = 0; i < 4; i++) {
        eggViews[i].resetState();
      }
    }

  public:
    int score;
    int speed;
    int miss;

    EggGame(EggView e[4], Wolf (&w), EggMover (&m)) : wolf(w), mover(m) {
      eggViews = e;
      score = 0;
      miss = 0;
      counter = 0;
      speed = startSpeed;
      inputBlocked = false;
      cock = false;
    }

    void init() {
      counter = 0;
      speed = startSpeed;
      score = 0;
      showScore();

      halfScoreIsShown = false;
      missAnimationTick = 0;

      miss = 0;
      showMiss();

      cock = false;   
      lastCockChange = 0;
      cockDuration = getCockDuration();
      showCock();

      wolf.initState();
      mover.onStart();
      resetEggViews();
      clearBrokenEggs();
    }

    void tick() {
      counter++;

      if(counter - missAnimationTick >= 4){
        animateMiss();
      }

      //показать/скрыть петуха
      if (counter - lastCockChange >= cockDuration) {
        cock = !cock;
        showCock();
        lastCockChange = counter;
        cockDuration = getCockDuration();
      }

      mover.tick(counter % 4, counter); //перемещение яиц

      for (int i = 0; i < 4; i++) {
        eggViews[mover.eggs[i].direction].showEgg(mover.eggs[i]); //отрисовка яиц

        //проверить, пойманы ли яйца, достигшие крайней точки
        if (mover.eggs[i].state == 5) {
          if (mover.eggs[i].isPicked(wolf.state)) {
            onEggCatch();
          } else {
            onEggMiss(mover.eggs[i].direction);
          }
        }
      }
    }

    void setWolfState(int s) {
      if (inputBlocked) {
        return;
      }

      wolf.setState(s);
      //обновить спрайты яиц в крайнем состоянии, т.к. они перекрываются со
      //спрайтами волка
      for (int i = 0; i < 4; i++) {
        if (mover.eggs[i].state == 3 || mover.eggs[i].state == 4) {
          eggViews[mover.eggs[i].direction].showEgg(mover.eggs[i]);
        }
      }
    }
};
