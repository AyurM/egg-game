class EggMover {
  private:
    int maxEggs = 1;
    int currentEggs = 0;
    int newEggChance = 33;
    int lastEggSpawnTick = 0;
    int minTicksBetweenEggs = 2;
    int ignoredDirection = 0;

    int findEmptyEggIndex() {
      for (int i = 0; i < 4; i++) {
        if (eggs[i].state == -1) {
          return i;
        }
      }
    }

    void clear() {
      currentEggs = 0;
      lastEggSpawnTick = 0;
      ignoredDirection = random(4);
      for (int i = 0; i < 4; i++) {
        eggs[i].reset();
      }
    }

  public:
    Egg *eggs;  //максимум на экране 4 яйца

    EggMover(Egg e[4]) {
      eggs = e;
    }

    void tick(int direction, int counter) {
      for (int i = 0; i < 4; i++) {
        //сбросить яйца в крайнем состоянии независимо
        //от направления
        if (eggs[i].state == 5) {
          eggs[i].reset();
          currentEggs--;
          continue;
        }
        if (direction != ignoredDirection) {
          //на каждом тике перемещаются только яйца, падающие
          //с направления direction
          if (eggs[i].direction != direction) {
            continue;
          }

          if (eggs[i].state < 5) {
            eggs[i].move();
          }
        }
      }

      //для неиспользуемого направления не генерировать яйца
      if (direction == ignoredDirection) {
        return;
      }

      //на каждом тике может заспавниться одно яйцо
      //с вероятностью newEggChance процентов
      if (currentEggs < maxEggs) {

        //между спавнами двух яиц должно пройти хотя бы minTicksBetweenEggs тиков
        if (counter - lastEggSpawnTick >= minTicksBetweenEggs) {
          if (random(100) <= newEggChance) {
            int index = findEmptyEggIndex();

            //не допустить спавн двух яиц подряд с одного направления
            for (int i = 0; i < 4; i++) {
              //проверка выполняется в момент спавна яйца, поэтому
              //соседним состоянием является состояние 1
              if (eggs[i].direction == direction && eggs[i].state == 1) {
                return;
              }
            }

            eggs[index].spawn(direction);
            lastEggSpawnTick = counter;
            currentEggs++;
          }
        }
      }
    }

    void onStart() {
      clear();
      maxEggs = 1;
    }

    void onMiss() {
      clear();
    }

    void increaseMaxEggs() {
      maxEggs++;
    }
};
