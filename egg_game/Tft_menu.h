//HMI Pictures
//Wolf
NexPicture wolf_left_pic = NexPicture(0, 3, "wolf_left");
NexPicture wolf_right_pic = NexPicture(0, 1, "wolf_right");

//Cock
NexPicture cock_pic = NexPicture(0, 44, "cock");

//Eggs
//Top Right
NexPicture egg_tr1_pic = NexPicture(0, 7, "egg_tr_1");
NexPicture egg_tr2_pic = NexPicture(0, 6, "egg_tr_2");
NexPicture egg_tr3_pic = NexPicture(0, 5, "egg_tr_3");
NexPicture egg_tr4_pic = NexPicture(0, 4, "egg_tr_4");
NexPicture egg_tr5_pic = NexPicture(0, 2, "egg_tr_5");

//Bottom Right
NexPicture egg_br1_pic = NexPicture(0, 12, "egg_br_1");
NexPicture egg_br2_pic = NexPicture(0, 11, "egg_br_2");
NexPicture egg_br3_pic = NexPicture(0, 10, "egg_br_3");
NexPicture egg_br4_pic = NexPicture(0, 9, "egg_br_4");
NexPicture egg_br5_pic = NexPicture(0, 8, "egg_br_5");

//Top Left
NexPicture egg_tl1_pic = NexPicture(0, 17, "egg_tl_1");
NexPicture egg_tl2_pic = NexPicture(0, 16, "egg_tl_2");
NexPicture egg_tl3_pic = NexPicture(0, 15, "egg_tl_3");
NexPicture egg_tl4_pic = NexPicture(0, 14, "egg_tl_4");
NexPicture egg_tl5_pic = NexPicture(0, 13, "egg_tl_5");

//Bottom Left
NexPicture egg_bl1_pic = NexPicture(0, 22, "egg_bl_1");
NexPicture egg_bl2_pic = NexPicture(0, 21, "egg_bl_2");
NexPicture egg_bl3_pic = NexPicture(0, 20, "egg_bl_3");
NexPicture egg_bl4_pic = NexPicture(0, 19, "egg_bl_4");
NexPicture egg_bl5_pic = NexPicture(0, 18, "egg_bl_5");

//Score
NexPicture digit_1_pic = NexPicture(0, 25, "digit_1");
NexPicture digit_2_pic = NexPicture(0, 24, "digit_2");
NexPicture digit_3_pic = NexPicture(0, 23, "digit_3");

//Miss
NexPicture miss_sign_pic = NexPicture(0, 26, "miss_sign");
NexPicture miss_1_pic = NexPicture(0, 27, "miss_1");
NexPicture miss_2_pic = NexPicture(0, 28, "miss_2");
NexPicture miss_3_pic = NexPicture(0, 29, "miss_3");

//Broken eggs
NexPicture breakl_1_pic = NexPicture(0, 30, "break_left_1");
NexPicture breakl_2_pic = NexPicture(0, 31, "break_left_2");
NexPicture breakl_3_pic = NexPicture(0, 32, "break_left_3");
NexPicture breakl_4_pic = NexPicture(0, 33, "break_left_4");
NexPicture breakl_5_pic = NexPicture(0, 34, "break_left_5");
NexPicture breakl_6_pic = NexPicture(0, 35, "break_left_6");
NexPicture breakl_7_pic = NexPicture(0, 36, "break_left_7");

NexPicture breakr_1_pic = NexPicture(0, 37, "break_right_1");
NexPicture breakr_2_pic = NexPicture(0, 38, "break_right_2");
NexPicture breakr_3_pic = NexPicture(0, 39, "break_right_3");
NexPicture breakr_4_pic = NexPicture(0, 40, "break_right_4");
NexPicture breakr_5_pic = NexPicture(0, 41, "break_right_5");
NexPicture breakr_6_pic = NexPicture(0, 42, "break_right_6");
NexPicture breakr_7_pic = NexPicture(0, 43, "break_right_7");

//Picture arrays
NexPicture eggs_top_left[5] = {egg_tl1_pic, egg_tl2_pic, egg_tl3_pic, egg_tl4_pic, egg_tl5_pic};
NexPicture eggs_bottom_left[5] = {egg_bl1_pic, egg_bl2_pic, egg_bl3_pic, egg_bl4_pic, egg_bl5_pic};
NexPicture eggs_top_right[5] = {egg_tr1_pic, egg_tr2_pic, egg_tr3_pic, egg_tr4_pic, egg_tr5_pic};
NexPicture eggs_bottom_right[5] = {egg_br1_pic, egg_br2_pic, egg_br3_pic, egg_br4_pic, egg_br5_pic};

//Sprite arrays
int wolf_sprites[4] = {0, 1, 2, 3};
int eggs_tl[5] = {17, 18, 19, 20, 21};
int eggs_tl_empty[5] = {47, 46, 45, 44, 43};
int eggs_bl[5] = {22, 23, 24, 25, 26};
int eggs_bl_empty[5] = {50, 49, 49, 49, 48};
int eggs_tr[5] = {7, 8, 9, 10, 11};
int eggs_tr_empty[5] = {57, 56, 55, 55, 54};
int eggs_br[5] = {12, 13, 14, 15, 16};
int eggs_br_empty[5] = {53, 52, 52, 52, 51};
int digits[11] = {27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37}; //11-я цифра - пустая
