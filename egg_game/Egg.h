class Egg {
  public:
    int direction;
    int state;

    Egg() {
      direction = 0;
      state = -1;
    }

    void spawn(int dir) {
      direction = dir;
      state = 0;
    }

    void reset() {
      state = -1;
    }

    void move() {
      if (state == -1) {
        return;
      }
      state++;
    }

    bool isPicked(int wolfState) {
      return wolfState == direction;
    }
};
