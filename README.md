# Egg Game

Эмулятор игры Egg для Nintendo Game & Watch на ESP8266 + дисплей TJC/Nextion

## Требования
Для работы необходима библиотека [ITEADLIB_Arduino_Nextion](https://github.com/itead/ITEADLIB_Arduino_Nextion)

Связь с дисплеем осуществляется через SoftwareSerial-соединение, настраиваемое путем правки файла
NexConfig.h:

```c++
/**
 * Define nexSerial for communicate with Nextion touch panel.
 */
//#define nexSerial Serial1

//Для Arduino UNO, Nano и т.д.
#include <SoftwareSerial.h>
extern SoftwareSerial SerialTJC;
#define nexSerial SerialTJC
```


